<?php
error_reporting ( E_ALL );
ini_set ( 'display_errors', 1 );

$options = getopt ( 'ed',['pdo:'] );
if (! $options)
	$options = array ();

define ( 'DEBUGGING', array_key_exists ( 'd', $options ) );

define ( 'EXECUTING', array_key_exists ( 'e', $options ) );

define ( 'takeaway_user', 'takeawayWrite' );
define ( 'takeaway_pass', '9P-Cb$1F' );
define ( 'takeaway_host', '10.53.1.1' );

$rst = new Restore ();
$rst->run ();

print "\nCOMPLETED!\n";


class Restore {

	private $sql;
	private $stmt_execute;


	public function __construct() {
		$this->takeaway_dns = 'mysql:dbname=thuis;host=' . takeaway_host;
		$this->takeawayDB = new PDO ( $this->takeaway_dns, takeaway_user, takeaway_pass, array (
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
		) );
		$this->takeawayDB->query ( 'SET NAMES utf8;' );

		$this->sql = file_get_contents('execute.sql');

		$this->stmt_execute = $this->takeawayDB->prepare ( $this->sql );
	}

	public function run() {
		print "Executing...\n";
		if(EXECUTING){
			$this->stmt_execute->execute ();
		}else{
			print "DRYRUN\n";
		}

		$this->printSql();
		print "\n";
	}

	/**
	 * Exclude the comments and empty lines
	 */
	public function printSql(){
		if ($file = fopen("execute.sql", "r")) {
			while(!feof($file)) {
				$line = trim(fgets($file));
				if( strpos($line, '#') === false && !empty($line) ){
					print "\t".$line."\r\n";
				}
			}
			fclose($file);
		}
	}
}


