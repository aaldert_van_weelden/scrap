<?php
/*
 * Insert debug functions
 * ____________________________________________________________________________________________
 */

$dumpCount = 0;
$instanceCount = 0;


function injectCSS(){
    print'
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style>
	body, pre{font-family:consolas;font-size:14px;}
	.xdebug-var-dump{display:inline;}
	h3{float:left;}
	.header{float:left;font-size:12px;position:relative;left:12px; top:3px;}

</style></head><body><h3><u>DEBUG</u></h3><div style="clear:both"></div>';
}

function dump($object, $caption=null, $halt=false){
    global $instanceCount;
    global $dumpCount;

    $dumpCount++;
    $ID = $dumpCount.'-'.$instanceCount;
    print '<br>';
    print '<span style="display:visible;cursor:pointer;" id="plus_'.$ID.'" onclick="
			document.getElementById(\'plus_'.$ID.'\').style.display=\'none\';
			document.getElementById(\'minus_'.$ID.'\').style.display=\'inline\';
			document.getElementById(\'dump_'.$ID.'\').style.display=\'block\';
		">[+]&nbsp;</span>';
    print '<span style="display:none;cursor:pointer;" id="minus_'.$ID.'" onclick="
			document.getElementById(\'plus_'.$ID.'\').style.display=\'inline\';
			document.getElementById(\'minus_'.$ID.'\').style.display=\'none\';
			document.getElementById(\'dump_'.$ID.'\').style.display=\'none\';
		">[-]&nbsp;</span>';
    if($caption!=null){
        print '<b>'.$caption.' : </b><br>';
    }
    print '<span style="display:none;" class="util_dump" id="dump_'.$ID.'">';
    print '<pre>';
    var_dump($object);
    print '</pre>';
    print '</span>';
    if($halt){
        exit();
    }
}

/*
 * _________________________________________________________________________________________________
 * Print debug from here
 */

injectCSS();

foreach ($GLOBALS as $key => $entry){
    global $instanceCount;
    dump($entry, $key);
    $instanceCount++;
}

die();