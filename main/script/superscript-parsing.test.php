<?php 
require_once '../lib/helpers.php';

injectCSS();

$map = [
		//lowercase
		7491 => [ 'superscript'=>'ᵃ', 'allergen' =>null],
		7495 => [ 'superscript'=>'ᵇ', 'allergen' =>null],
		7580 => [ 'superscript'=>'ᶜ', 'allergen' =>null],
		7496 => [ 'superscript'=>'ᵈ', 'allergen' =>null],
		7497 => [ 'superscript'=>'ᵉ', 'allergen' =>null],
		7584 => [ 'superscript'=>'ᶠ', 'allergen' =>null],
		7501 => [ 'superscript'=>'ᵍ', 'allergen' =>null],
		688 =>  [ 'superscript'=>'ʰ', 'allergen' =>null],
		8305 => [ 'superscript'=>'ⁱ', 'allergen' =>null],
		690 =>  [ 'superscript'=>'ʲ', 'allergen' =>'P'],
		7503 => [ 'superscript'=>'ᵏ', 'allergen' =>'O'],
		737 =>  [ 'superscript'=>'ˡ', 'allergen' =>null],
		7504 => [ 'superscript'=>'ᵐ', 'allergen' =>'N'],
		8319 => [ 'superscript'=>'ⁿ', 'allergen' =>'H'],
		7506 => [ 'superscript'=>'ᵒ', 'allergen' =>null],
		7510 => [ 'superscript'=>'ᵖ', 'allergen' =>'D'],
		691 =>  [ 'superscript'=>'ʳ', 'allergen' =>'B'],
		738 =>  [ 'superscript'=>'ˢ', 'allergen' =>'L'],
		7511 => [ 'superscript'=>'ᵗ', 'allergen' =>'F'],
		7512 => [ 'superscript'=>'ᵘ', 'allergen' =>'M'],
		7515 => [ 'superscript'=>'ᵛ', 'allergen' =>'G'],
		695 =>  [ 'superscript'=>'ʷ', 'allergen' =>null],
		739 =>  [ 'superscript'=>'ˣ', 'allergen' =>'C'],
		696 =>  [ 'superscript'=>'ʸ', 'allergen' =>'A'],
		7611 => [ 'superscript'=>'ᶻ', 'allergen' =>null],
		
		//uppercase
		7468 => [ 'superscript'=>'ᴬ', 'allergen' =>null],
		7470 => [ 'superscript'=>'ᴮ', 'allergen' =>null],
		7472 => [ 'superscript'=>'ᴰ', 'allergen' =>null],
		7473 => [ 'superscript'=>'ᴱ', 'allergen' =>null],
		7475 => [ 'superscript'=>'ᴳ', 'allergen' =>null],
		7476 => [ 'superscript'=>'ᴴ', 'allergen' =>null],
		7477 => [ 'superscript'=>'ᴵ', 'allergen' =>null],
		7478 => [ 'superscript'=>'ᴶ', 'allergen' =>null],
		7479 => [ 'superscript'=>'ᴷ', 'allergen' =>null],
		7480 => [ 'superscript'=>'ᴸ', 'allergen' =>null],
		7481 => [ 'superscript'=>'ᴹ', 'allergen' =>null],
		7482 => [ 'superscript'=>'ᴺ', 'allergen' =>null],
		7484 => [ 'superscript'=>'ᴼ', 'allergen' =>null],
		7486 => [ 'superscript'=>'ᴾ', 'allergen' =>null],
		7487 => [ 'superscript'=>'ᴿ', 'allergen' =>null],
		7488 => [ 'superscript'=>'ᵀ', 'allergen' =>null],
		7489 => [ 'superscript'=>'ᵁ', 'allergen' =>null],
		11389 => [ 'superscript'=>'ⱽ', 'allergen' =>null],
		7490 => [ 'superscript'=>'ᵂ', 'allergen' =>null],
		
		//numbers
		8304 => [ 'superscript'=>'⁰', 'allergen' =>null],
		185 =>  [ 'superscript'=>'¹', 'allergen' =>'6'],
		178 =>  [ 'superscript'=>'²', 'allergen' =>'10'],
		179 =>  [ 'superscript'=>'³', 'allergen' =>'3'],
		8308 => [ 'superscript'=>'⁴', 'allergen' =>'1'],
		8309 => [ 'superscript'=>'⁵', 'allergen' =>'4'],
		8310 => [ 'superscript'=>'⁶', 'allergen' =>'2'],
		8311 => [ 'superscript'=>'⁷', 'allergen' =>'8'],
		8312 => [ 'superscript'=>'⁸', 'allergen' =>'9'],
		8313 => [ 'superscript'=>'⁹', 'allergen' =>'9.1'],
		
		7475 => [ 'superscript'=>'ᴳ', 'allergen' =>null],//detected
		7473 => [ 'superscript'=>'ᴱ', 'allergen' =>null],//detected
		
];


class ParseResult{
	public $allergens;
	public $cleaned;
	public $unmapped;
	
	public function __construct(){
		$this->allergens = [];
		$this->cleaned = [];
		$this->unmapped = [];
	}
	
	public function serialize(){
		$this->cleaned = trim(implode( $this->cleaned));
		$this->allergens = trim(implode(",", $this->allergens));
		$this->unmapped = trim(implode(",", $this->unmapped));
	}
}

function parseSuperscript($in, $sender ='', $map){
	$result = new ParseResult();
	if(isNotNullOrEmpty($in)){
		//check if the ord is present in the map keys
		$pointer = 0;
		$i=0;
		while(($chr = nextchar($in, $pointer)) !== false){
			//TODO
			$ord = unistr_to_ords( $chr )[0];
			$superscript_ords = array_keys($map);
			if(in_array($ord, $superscript_ords)){
				$i++;
				//store mapped value if present
				if($map[$ord]['allergen'] !==null ) {
					array_push($result->allergens, $map[$ord]['allergen']);
				}else{
					//superscript detected, but no TMS code known, so store the unicode value
					array_push($result->unmapped, $ord);
				}
			}else{
				//store original unicode character
				array_push($result->cleaned, $chr);
			}
		}
		
		out("\t\t$i occurences of Superscript detected in $sender");
		
	}
	return $result;
}


function nextchar($string, &$pointer){
	if(!isset($string[$pointer])) return false;
	$char = ord($string[$pointer]);
	if($char < 128){
		return $string[$pointer++];
	}else{
		if($char < 224){
			$bytes = 2;
		}elseif($char < 240){
			$bytes = 3;
		}elseif($char < 248){
			$bytes = 4;
		}elseif($char == 252){
			$bytes = 5;
		}else{
			$bytes = 6;
		}
		$str =  substr($string, $pointer, $bytes);
		$pointer += $bytes;
		return $str;
	}
}

function unistr_to_ords($str, $encoding = 'UTF-8'){
	// Turns a string of unicode characters into an array of ordinal values,
	// Even if some of those characters are multibyte.
	$str = mb_convert_encoding($str,"UCS-4BE",$encoding);
	$ords = array();
	
	// Visit each unicode character
	for($i = 0; $i < mb_strlen($str,"UCS-4BE"); $i++){
		// Now we have 4 bytes. Find their total
		// numeric value.
		$s2 = mb_substr($str,$i,1,"UCS-4BE");
		$val = unpack("N",$s2);
		$ords[] = $val[1];
	}
	return($ords);
}

function isNotNullOrEmpty($in, $property = null, $zeroAllowed = false) {
	if ($property === null) {
		
		if (is_object ( $in ) && count ( (array) $in ) === 0 )
			return false;
			$isNotEmpty = $zeroAllowed ? ! empty ( $in ) : $in === 0 || $in === '0' || ! empty ( $in );
			if (is_string ( $in )) {
				return $isNotEmpty && strtoupper ( $in ) !== 'NULL' && strtoupper ( $in ) !== 'FALSE' && strtoupper ( $in ) !== 'UNDEFINED';
			} else {
				return $isNotEmpty;
			}
	}
	
	if (! is_object ( $in ))
		throw new Exception ( 'Provided test $in must be a class instance(object) or a valid classname(string)' );
		if (! property_exists ( $in, $property ))
			return false;
			$prop = $in->{$property};
			$isNotEmpty = $zeroAllowed ? ! empty ( $prop ) : $prop === 0 || $prop === '0' || ! empty ( $prop );
			if (is_string ( $prop )) {
				return $isNotEmpty && strtoupper ( $prop ) !== 'NULL' && strtoupper ( $prop ) !== 'FALSE' && strtoupper ( $prop ) !== 'UNDEFINED';
			} else {
				return $isNotEmpty;
			}
}


$error_test = "8 Pizzabrötchenʸ mit Edamer⁴ ᵛ und Kräuterbutter⁴ ⁶ ᵛ";

$out = parseSuperscript($error_test,'naam', $map);

dump($out);






