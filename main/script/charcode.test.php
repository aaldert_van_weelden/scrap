<?php 
require_once '../lib/helpers.php';

injectCSS();

$text = '⁴';

$supscript_number_set = "⁰¹²³⁴⁵⁶⁷⁸⁹";

$supscript_lowercase_set = "ᵃᵇᶜᵈᵉᶠᵍʰⁱʲᵏˡᵐⁿᵒᵖʳˢᵗᵘᵛʷˣʸᶻ";

$supscript_uppercase_set = "ᴬᴮᴰᴱᴳᴴᴵᴶᴷᴸᴹᴺᴼᴾᴿᵀᵁⱽᵂ";


$error_test = "8     Pizzabrötchenʸ    mit   Edamer⁴ ᵛ und Kräuterbutter⁴ ⁶ ᵛ";




dump($text);



function ordutf8($string, $offset) {
	$code = ord(substr($string, $offset,1));
	if ($code >= 128) {        //otherwise 0xxxxxxx
		if ($code < 224) $bytesnumber = 2;                //110xxxxx
		else if ($code < 240) $bytesnumber = 3;        //1110xxxx
		else if ($code < 248) $bytesnumber = 4;    //11110xxx
		$codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) - ($bytesnumber > 3 ? 16 : 0);
		for ($i = 2; $i <= $bytesnumber; $i++) {
			$offset ++;
			$code2 = ord(substr($string, $offset, 1)) - 128;        //10xxxxxx
			$codetemp = $codetemp*64 + $code2;
		}
		$code = $codetemp;
	}
	$offset += 1;
	if ($offset >= strlen($string)) $offset = -1;
	return $code;
}




dump(ordutf8($text, 0) , 'ordutf8: utf8 code');




function ords_to_unistr($ords, $encoding = 'UTF-8'){
	// Turns an array of ordinal values into a string of unicode characters
	$str = '';
	for($i = 0; $i < sizeof($ords); $i++){
		// Pack this number into a 4-byte string
		// (Or multiple one-byte strings, depending on context.)
		$v = $ords[$i];
		$str .= pack("N",$v);
	}
	$str = mb_convert_encoding($str,$encoding,"UCS-4BE");
	return(utf8_encode($str) );
}

function unistr_to_ords($str, $encoding = 'UTF-8'){
	// Turns a string of unicode characters into an array of ordinal values,
	// Even if some of those characters are multibyte.
	$str = mb_convert_encoding($str,"UCS-4BE",$encoding);
	$ords = array();
	
	// Visit each unicode character
	for($i = 0; $i < mb_strlen($str,"UCS-4BE"); $i++){
		// Now we have 4 bytes. Find their total
		// numeric value.
		$s2 = mb_substr($str,$i,1,"UCS-4BE");
		$val = unpack("N",$s2);
		$ords[] = $val[1];
	}
	return($ords);
}


function nextchar($string, &$pointer){
	if(!isset($string[$pointer])) return false;
	$char = ord($string[$pointer]);
	if($char < 128){
		return $string[$pointer++];
	}else{
		if($char < 224){
			$bytes = 2;
		}elseif($char < 240){
			$bytes = 3;
		}elseif($char < 248){
			$bytes = 4;
		}elseif($char == 252){
			$bytes = 5;
		}else{
			$bytes = 6;
		}
		$str =  substr($string, $pointer, $bytes);
		$pointer += $bytes;
		return $str;
	}
}


dump(ords_to_unistr([8303]), 'ords_to_unistr');

dump(unistr_to_ords($text), 'unistr_to_ords');


//================

out('iterate the utf8 error string and fetch charcode...');
$pointer = 0;

while(($chr = nextchar($error_test, $pointer)) !== false){
	
	print unistr_to_ords( $chr )[0]." | ";
	
}

out('iterate the utf8 error string and fetch charcode...');
$pointer = 0;

while(($chr = nextchar($error_test, $pointer)) !== false){
	
	print $chr." | ";
	
	
}



//=============== generate the superscript array

// out('iterate the utf8 string and fetch charcode...');
// $pointer = 0;
// print '[ ';
// while(($chr = nextchar($supscript_lowercase_set.$supscript_uppercase_set.$supscript_number_set, $pointer)) !== false){

// 	print unistr_to_ords( $chr )[0]." => [ 'superscript'=>'".$chr."', 'allergen' =>'?'], <br>";

// }
// print ']';


//=========================

out('remove spaces');

$test = "  Edammer    kaas   met     broodje   ";

$aggr = [];
$pointer = 0;

while(($chr = nextchar($test, $pointer)) !== false){
	
	array_push($aggr, $chr);
	
}



$raw = implode($aggr);
dump($raw);

$words = explode(" ",$raw);

dump($words); 

$out = '';
foreach ($words as $word){

	if( !empty($word)){
		$out.= trim($word);
		$out .= " ";
	}
}

$out = trim($out);

dump($out);







