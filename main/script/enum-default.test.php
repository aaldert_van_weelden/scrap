<?php
require_once '../lib/helpers.php';
require_once '../lib/Enum.php';

use Base\Enum;
use Base\ENUM_PARAM;

injectCSS();

class Type extends Enum{

	const FIRST = 'first_value';//ordinal 0
	const SECOND = 'second_value';//ordinal 1
	const THIRD = 'third_value';//ordinal 2



	public $default = self::FIRST;

	/* (non-PHPdoc)
     * @see EnumInterface::get()
    */
    public static function get($value = null, $default = ENUM_PARAM::ADD_DEFAULT_VALUE){
        return new Type($value, $default);
    }

}






$type = Type::get(Type::FIRST,ENUM_PARAM::NO_DEFAULT_VALUE);

dump($type, 'the enumeration');

try {
    dump( Type::get(null, ENUM_PARAM::NO_DEFAULT_VALUE));
} catch (Exception $e) {
    out('does not work, Expected exception: '.$e->getMessage());
}


$type = Type::get(Type::FIRST,true);

dump($type, 'the enumeration with default');

dump( Type::get(null)->ordinal(), 'this works' );


?>