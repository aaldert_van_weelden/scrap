<?php
require_once '../../lib/helpers.php';

injectCSS();


class Test{


	/**
	 * The @name annotation maps the DB fieldname to the property, and @var provides the PDO data type
	 *
	 * @uses setMapping
	 */

	/**
	 * Incremental database id
	 *
	 * @name id
	 * @var string
	 */
	public $id;

	/**
	 * Creation datetime Y-m-d H:i:s
	 *
	 * @name timestamp
	 * @var string
	 */
	public $timestamp;

	/**
	 * PHP session
	 *
	 * @name session_id
	 * @var string
	 */
	public $sessionId;

	/**
	 * Client IP address
	 *
	 * @name source_ip
	 * @var string
	 */
	public $sourceIP;

	/**
	 * Application name uppercase
	 *
	 * @name application
	 * @var string
	 */
	public $application;

	/**
	 * Url from HTTP_REFERER
	 *
	 * @name url
	 * @var string
	 */
	public $url;

	/**
	 * The fired event
	 *
	 * @name event
	 * @var string
	 */
	public $event;

	/**
	 * User id available when logged in
	 *
	 * @name user_id
	 * @var integer
	 */
	public $userId;

	/**
	 * Mandatory username
	 *
	 * @name username
	 * @var string
	 */
	public $userName;

	/**
	 * Restaurant id
	 *
	 * @name rest_id
	 * @var integer
	 */
	public $restId;

	/**
	 *
	 * @name country_id
	 * @var integer
	 */
	public $countryId;

	/**
	 * Payment account
	 *
	 * @name accountnumber
	 * @var string
	 */
	public $accountNumber;

	/**
	 * Customer account name
	 *
	 * @name accountname
	 * @var string
	 */
	public $accountName;

	/**
	 *
	 * @name order_id
	 * @var integer
	 */
	public $orderId;

	/**
	 *
	 * @name invoice_id
	 * @var integer
	 */
	public $invoiceId;

	/**
	 * Time to live timestamp Y-m-d H:i:s
	 *
	 * @name ttl
	 * @var string
	 */
	public $ttl;

	/**
	 * Keep as evidence when TRUE
	 *
	 * @name keep
	 * @var boolean
	 */
	public $keep;

	/**
	 * Optional event message
	 *
	 * @name message
	 * @var string
	 */
	public $message;

	/**
	 * Optional event data
	 *
	 * @name data
	 * @var string
	 */
	public $data;

	/**
	 * Optional event amount
	 *
	 * @name amount
	 * @var integer
	 */
	public $amount;


	private $threshold;

	/**
	 * DTO property to DB field mapping
	 *
	 * @var array
	 */
	private $map;

	/**
	 * property data types
	 *
	 * @var array
	 */
	private $types;



	public function __construct(){
		$this->userName = 'TestUsername';
		$this->sessionId = '123456';
		$this->accountNumber = '45';
		$this->accountName = 'test';
		$this->restId = 9999;
		$this->threshold = 5;

		$this->setMapping();
	}

	public function run(){

		$sql ='';
		$message = '';
		$params = '';
		$criterium = '';

		dump($this->checkLoginsByUsername(),'Builded query'.__LINE__);//OK

		dump($this->checkRefundByUserAndAccountNumber(),'Builded query'.__LINE__);

		dump($this->checkMorethenOneParam(),'Builded query'.__LINE__);

	}



	public function checkLoginsByUsername() {
		print __FUNCTION__.'<br>';
		$message = "FAILED LOGIN ATTEMPTS EXCEEDED $this->threshold FOR USER $this->userName";

		return $this->buildQueryAndExecute("select count(*) as count", $message, [
			'userName'
		], $this->threshold);
	}


	public function checkRefundByUserAndAccountNumber() {
		print __FUNCTION__.'<br>';
		if (empty($this->accountNumber))
			return false;
			$message = "REFUND ATTEMPTS BY USER $this->userName EXCEEDED $this->threshold FOR ACCOUNTt $this->accountNumber";

			return $this->buildQueryAndExecute("select count(*) as count", $message, [
				'userName',
				'accountNumber'
			], $this->threshold);
	}



	public function checkMorethenOneParam() {
		print __FUNCTION__.'<br>';
		$message = "UNAUTHORIZED SETTING CHANGES BY USER $this->userName EXCEEDED $this->threshold FOR RESTAURANT $this->restId";

		return $this->buildQueryAndExecute("select count(*) as count", $message, [
			'userName',
			'restId',
			'accountName'
		], $this->threshold);
	}

	private function buildQueryAndExecute($sql, $message, array $params = [], $criterium) {
		$sql .= ", group_concat(id) as ids from log_monitoring where";

		foreach ($params as $prop) {
			$sql .= " " . array_flip($this->map)[$prop] . " = :$prop";
			$sql .= " and";
		}

		// always filter on event and valid timewindow
		$sql .= " event = :event and ttl>now()";

		return $sql;
	}


	/**
	 * Create the propertyname to DB field mapping and the propertyname to PDO datatype mapping<br>
	 * This methods scans the PHPdoc provided with the property declaration.
	 * The @name annotation provides the DB fieldname to map to
	 */
	private function setMapping() {
		$refClass = new ReflectionClass($this);

		foreach ($refClass->getProperties() as $refProperty) {
			if (preg_match('/@var\s+([^\s]+)/', $refProperty->getDocComment(), $matches)) {
				list (, $type) = $matches;
				if (in_array($type, [
					'array',
					'object'
				])) {
					continue;
				}
			}
			if (preg_match('/@name\s+([^\s]+)/', $refProperty->getDocComment(), $matches)) {
				list (, $field) = $matches;
			}
			else {
				continue;
			}
			$this->map[$field] = $refProperty->name;

			switch ($type) {
				case 'integer':
				case 'int':
					$this->types[$field] = PDO::PARAM_INT;
					break;
				case 'boolean':
				case 'bool':
					$this->types[$field] = PDO::PARAM_BOOL;
					break;
				case 'NULL':
					$this->types[$field] = PDO::PARAM_NULL;
					break;
				default:
					$this->types[$field] = PDO::PARAM_STR;
					break;
			}
		}
	}

	/**
	 * Return the transition object if available, use
	 *
	 * @param object $timezone
	 */
	private function timezone($timezone) {
		$tz = new DateTimeZone($timezone);
		$transitions = $tz->getTransitions();
		if (is_array($transitions)) {
			foreach ($transitions as $k => $t) {
				// look for current year
				if (substr($t['time'], 0, 4) == date('Y')) {
					$trans = $t;
					break;
				}
			}
		}
		return (isset($trans)) ? (object) $trans : false;
	}

	/**
	 * Return the offset in seconds
	 * Returns null if the DST is not available for detecting (PHP version < 5.2)
	 */
	private function getOffset($dateTimezone = self::DATETIMEZONE) {
		$tz = $this->timezone($dateTimezone);
		if (is_object($tz)) {
			$sign = (integer) $tz->offset < 0 ? "-" : "+";
			$hour = str_pad(floor(abs($tz->offset) / 3600), 2, "0", STR_PAD_LEFT);

			return $sign . $hour . '00';
		}
		return '+0000';
	}
}

$test = new Test;
$test->run();