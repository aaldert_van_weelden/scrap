<?php
use Base\ENUM_PARAM;

require_once '../lib/helpers.php';
require_once '../lib/Enum.php';

injectCSS();

class Type extends Base\Enum{

	const FIRST ='first_value';
	const SECOND = 'second_value';
	const THIRD = 'third_value';
	const FOURTH = 'fourth_value';

	public $default = self::FIRST;

	/* (non-PHPdoc)
	 * @see EnumInterface::get()
	 */
	public static function get($value = null, $default = ENUM_PARAM::ADD_DEFAULT_VALUE){
	    return new Type($value, $default);
	}

	/**
	 * @Override
	 * {@inheritDoc}
	 * @see \Base\Enum::map()
	 */
	function map(){

		if(self::$map===null){
    		self::$map = [
    				self::FIRST => '10',
    				self::SECOND => '100',
    				self::THIRD => '1000',
    				self::FOURTH => '10000'
    		];
		}
		return $this->mapping();
	}
}


class Nomap extends Base\Enum{

	const ONE = false;
	const TWO = 'SECOND_VALUE';

	public $default = self::ONE;

	/* (non-PHPdoc)
	 * @see EnumInterface::get()
	 */
	public static function get($value = null, $default = ENUM_PARAM::ADD_DEFAULT_VALUE){
	    return new Nomap($value, $default);
	}
}





out('* Creating Type::THIRD');

$type = Type::get(Type::THIRD);

dump($type->constants(), 'the key->value pairs');

out($type->value(),'the value');
out($type->name(),'the name');
out($type->ordinal(),'ordinal');


out('* Trying to map the enum keys:');

dump($type->mapping());

out('* Get enum mapped SECOND value');

dump( Type::get(Type::SECOND )->mapped() );



//test mapchecking
try {
    $nomap = Nomap::get(Nomap::ONE);
    dump($nomap->mapped());//expect to throw exception
} catch (Exception $e) {
    out('$nomap->mapped() throws exception as expected: '.$e->getMessage());
}

?>