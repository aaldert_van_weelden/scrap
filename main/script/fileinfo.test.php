<?php
require_once '../lib/helpers.php';

injectCSS();



function getFileInfo($name) {

	$parts = explode(".", $name);
	$ext = '';
	if (count($parts) > 1) {
		$ext = trim( $parts[count($parts) - 1] );
		unset($parts[count($parts) - 1]);
		$name = implode(".", $parts);
	}

	if(strpos($name, '/') !== false){
		$parts = explode('/', $name);
		$name = $parts[count($parts)-1];
	}

	// remove unwanted hyphens from the filename here
	$name = str_replace("--", "", $name);
	$name = ltrim($name, "-");
	$name = trim( rtrim($name, "-") );

	if(empty($name)) return false;

	return (object) [
		'name' => $name,
		'ext' => $ext
	];
}


$name = '';
dump(getFileInfo($name));

$name = 'test.jpg';
dump(getFileInfo($name));

$name = 'bla.test.jpg';
dump(getFileInfo($name));

$name = 'restaurant/12345/blah.test.png';
dump(getFileInfo($name));

$name = '/test.jpg';
dump(getFileInfo($name));

$name = '345/test.jpg';
dump(getFileInfo($name));




