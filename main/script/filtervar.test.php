<?php
require_once '../lib/helpers.php';

injectCSS();

$in = '12';

$out = filter_var($in, FILTER_VALIDATE_INT)!==false;

dump($out);

$id =  (integer) "123456"."002";

$id*=-1;

dump($id);

$in = "";
dump( (integer) $in);


$in = '1';

$out = filter_var($in, FILTER_VALIDATE_BOOLEAN);

dump($out);

$in = '0';

$out = filter_var($in, FILTER_VALIDATE_BOOLEAN);

dump($out);



$out = filter_var($_POST['unset'], FILTER_VALIDATE_BOOLEAN);

dump($out);

$userId = '45';

if($userId = filter_var($userId, FILTER_VALIDATE_INT)){
	dump($userId);
}else{
	out('could not parse the userId to a valid integer');
}

$date = "2018-12-24";

if($date = filter_var($date, FILTER_VALIDATE_REGEXP, ['options'=>['regexp'=>'([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))']])){
	dump($date);
}else{
	out('could not parse the date to a valid date');
}



