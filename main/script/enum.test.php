<?php
require_once '../lib/helpers.php';
require_once '../lib/Enum.php';

use Base\Enum;
use Base\ENUM_PARAM;

injectCSS();

class Type extends Enum{

	const FIRST = 'first_value';//ordinal 0
	const SECOND = 'second_value';//ordinal 1
	const THIRD = 'third_value';//ordinal 2



	public $default = self::FIRST;

	/* (non-PHPdoc)
     * @see EnumInterface::get()
    */
    public static function get($value = null, $default = true){
        return new Type($value, $default);
    }

}


$type = Type::get(Type::THIRD,false);

dump($type->constants(), 'the enumeration list');

out($type->value(),'the value of Type::THIRD');
out($type->name(),'the name of Type::THIRD');
out($type->ordinal(),'ordinal of Type::THIRD');


$newType = Type::get()->byOrdinal(1);

dump(  $newType,  'type by ordinal 1'  );

out($newType->name(), 'type by ordinal 1 name');




out('* Translations:');

$type = Type::get('third_value');

$type->setI18n('third_translation_by_stringparameter');

dump($type, 'translation by hardcoded string');


$translations = [
    'FIRST'=> 'first_translation_by_name',
    'SECOND'=> 'second_translation_by_name',
    'THIRD'=> 'third_translation_by_name',
];

//default by name
$type->setI18n($translations);

dump($type, 'translation by name as default');

$translations_by_value = [
    Type::FIRST=> 'first_translation_by_value',
    Type::SECOND=> 'second_translation_by_value',
    Type::THIRD=> 'third_translation_by_value',
];

//default by value
$type->setI18n($translations_by_value, ENUM_PARAM::TRANSLATE_BY_VALUE);

dump($type, 'translation by value when flag = TRUE');




out('* switch example:');
$test = 'second_value';

switch($test){
    case Type::FIRST:
        out(0);
        break;
    case Type::SECOND:
        out(1, 'Detected SECOND');
        break;
    case Type::THIRD:
        out(2);
        break;
}




out('* view example:');

$vars = ['type'=>$type];

out($vars['type']->i18nValue, 'translation');
out($vars['type']->name(), 'name');
out($vars['type']->value(),'value');
out($vars['type']->ordinal(), 'ordinal');


?>