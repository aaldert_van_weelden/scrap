<?php
require_once '../lib/helpers.php';

injectCSS();


class SecurityLogDTO{

    /**
     * Data transport container constructor
     * @param string $username
     * @param integer $userId
     * @param integer $countryId
     */
    public function __construct($username, $userId = null, $countryId = null){
        $this->username = $username;
        $this->userId = $userId;
        $this->countryId = $countryId;
    }

    public $username;
    public $userId;
    public $restId;
    public $countryId;
    public $accountNumber;
    public $accountName;
    public $orderId;
    public $invoiceId;
}

class event{

    public static $events = [];

    public static function trigger($event, $args = array()){

        if(isset(self::$events[$event])){

            foreach(self::$events[$event] as $func){

                call_user_func($func, $args);
            }
        }
    }

    public static function bind($event, Closure $func){

        self::$events[$event][] = $func;
    }
}


event::bind('test', function( SecurityLogDTO $args){
    dump($args, 'callback arguments');
});


$args = new SecurityLogDTO('van weelden', 2345, 6543);

out('firing test event...');

event::trigger('test', $args);