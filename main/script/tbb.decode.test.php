<?php
require_once '../lib/helpers.php';

injectCSS();


$kenmerk = 'MU2SK4';
$in= 'ON628Q0NQ44';

function tdmIdEncode($id) {
    $result = str_replace(array("1","3","5","7","9"), array("A","B","C","D","E"), $id);
    return 'O' . str_rot13($result); // add "O" prefix to differentiate from old numeric IDs
}

function tdmIdDecode($id) {
    $result = str_rot13(substr($id, 1)); // remove "O" prefix
    return str_replace(array("A","B","C","D","E"),array("1","3","5","7","9"), $result);
}


$out = tdmIdDecode($in);

dump( $out );

dump(tdmIdEncode($out) );

