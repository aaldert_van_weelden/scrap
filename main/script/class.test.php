<?php
require_once '../lib/helpers.php';

injectCSS();


class Test{

	const STATUS_OPEN 			= 0;
	const STATUS_CLOSED 		= 1;
	const STATUS_DONE 			= 2;
	const STATUS_IN_PROGRESS	= 3;
	const STATUS_ON_HOLD 		= 4;
	const STATUS_WONT_DO 		= 5;

	//TODO replace with enum
	private $statusEnum = [
		self::STATUS_OPEN 		=> 'STATUS_OPEN',
		self::STATUS_CLOSED 	=> 'STATUS_CLOSED',
		self::STATUS_DONE 		=> 'STATUS_DONE',
		self::STATUS_IN_PROGRESS=> 'STATUS_IN_PROGRESS',
		self::STATUS_ON_HOLD 	=> 'STATUS_ON_HOLD',
		self::STATUS_WONT_DO 	=> 'STATUS_WONT_DO'
	];


	public function __construct(){

	}

	public function run(){
		dump($this->statusEnum,'Status Enum array');
	}
}

$test = new Test;
$test->run();

$in = '0';
//$in = 0;

switch($in){
	case 0:
		out('zero detected!');
		break;
	default:
		out('type wrong');
		break;
}

out('--------------------- dynamic class ---------------------------------------');

$myclassname = "anewclassname";

eval("class {$myclassname} { }");
// With a property
$myclassname = "anothernewclassname";
$myproperty = "newproperty";
eval("class {$myclassname} { protected \${$myproperty}; }");

$test = new anothernewclassname();

dump($test);

$test = (object) [];

dump(get_class($test));




