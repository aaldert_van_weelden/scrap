<?php
require_once '../lib/helpers.php';

error_reporting(E_STRICT);

injectCSS();


$a = [
		12,13,14,15,16,17,18,19, 20, 45,56,67
];

$start = 19;

$out = [];

foreach ($a as $id){
	if($id >= $start){
		$out[] = $id;
	}
}

dump($out);

out('--------------- array_diff_key ---------------------');

$update = [
	124 => "Rijst vegetarisch",
	125 =>"Rijst kerrie kip",
	126 => "Rijst kipfilet",
	127 =>"Rijst lamsfilet",
	128 =>"Rijst bruine bonen met kip",
];

$keep = [
	125 => "on",
	126 => "on",
];

$diff = array_intersect_key($update, $keep);

$ffid = array_intersect_key($keep, $update);

dump($diff);

dump($ffid);


out('---------------------- array values by reference ----------------------');

$in = [1,2,3,4,'7',0, 'test'];

foreach ($in as &$i){
	$i = filter_var($i, FILTER_VALIDATE_INT);
}
unset($i);

dump($in);

out('------------------------   truncate ------------------------------------');

$in = [1,2,3,4,5,6,7,8,9];

$out = array_splice($in, 0, 3);

dump($out);

out('------------------------   unique ------------------------------------');

$in = '1,2,3,3,3,4,2';

$in = explode(',', $in);

$out = array_unique($in);

dump($out);
dump(count($out));

$in = '1,1,1,1,2';

$in = explode(',', $in);

$out = array_unique($in);

dump($out);
dump(count($out));

$in = null;

$in = explode(',', $in);

$out = array_unique($in);

dump($out);
dump(count($out));


out('------------------------   unique values------------------------------------');

$in = '1,2,3,3,3,4,2';

$in = explode(',', $in);

$out = array_unique($in);

dump($out);
dump(count($in));
dump(count($out));

out('------------------------  detect false flag ------------------------------------');

$in = [true,true,true];

$in2 = [true,true,false];



dump(array_unique($in));

dump(array_unique($in2));

dump( in_array(false,$in));

dump( in_array(false,$in2));






