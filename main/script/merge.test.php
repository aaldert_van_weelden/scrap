<?php
require_once '../lib/helpers.php';

injectCSS();

$old = "k,b,g";

$new = "m,b,h";

$merged = implode ( ",", array_unique( array_merge( explode(",", $old), explode(",", $new) ) )  );

dump($merged , 'merged');


$old = null;

$new = "m,b,h";

$merged = implode ( ",", array_unique( array_merge( explode(",", $old), explode(",", $new) ) )  );

dump($merged , 'merged');


$one = [1,2,3];
$two = [6,7,8];

$merged = array_merge($one, $two);

dump($merged, 'merged one and two');

$merged = array_merge($two, $one);

dump($merged, 'merged two and one');

 out('----------------------- merge grouped arrays -------------------------------------------<pre>');

 $res = [];

 $arr1 = ['abc' =>[3,5,7], 'def'=>[2,4,6]];

 $res = array_merge_recursive($res, $arr1);

 $arr2 = ['abc' =>[13,15], 'def'=>[2,4,9]];

 $res = array_merge_recursive($res, $arr2);

 $arr3 = ['klm' =>[20,22], 'def'=>[33,34]];

 $res = array_merge_recursive($res, $arr3);



 foreach($res as $key => &$grouped){
 	$grouped = array_unique($grouped);
 }
 unset($grouped);

 dump(count($res));
 dump($res);


 out('----------------------------------- chunk array ------------------------------------------------');

 $arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18];

 $chunks = array_chunk($arr, 4);

 dump(count($chunks));

 foreach ($chunks as $chunk){
 	dump($chunk);
 	dump( implode(',',$chunk));
 }


