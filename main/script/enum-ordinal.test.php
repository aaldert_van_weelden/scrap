<?php
require_once '../lib/helpers.php';
require_once '../lib/Enum.php';

use Base\Enum;
use Base\ENUM_PARAM;

injectCSS();

class Type extends Enum{

	const FIRST = 'first_value';//ordinal 0
	const SECOND = 'second_value';//ordinal 1
	const THIRD = 'third_value';//ordinal 2



	public $default = self::FIRST;

	/* (non-PHPdoc)
     * @see EnumInterface::get()
    */
    public static function get($value = null, $default = ENUM_PARAM::ADD_DEFAULT_VALUE){
        return new Type($value, $default);
    }

}

//no default value
$type = Type::get(Type::THIRD,ENUM_PARAM::NO_DEFAULT_VALUE);

dump($type->constants(), 'the enumeration list');

out('* create new enum');

out($type->value(),'the value of Type::THIRD',4);
out($type->name(),'the name of Type::THIRD',4);
out($type->ordinal(),'ordinal of Type::THIRD',4);


$newType = Type::get()->byOrdinal(1);

dump_c(  $newType,  'type by ordinal 1 dump'  );

out($newType->name(), 'type by ordinal 1 name',4);



out('* Propagation of "one enum instance with value set" example:');

$type = Type::get('third_value');

out('* view example:');

$vars = ['type'=>$type];

out($vars['type']->name(), 'name',4);
out($vars['type']->value(),'value',4);
out($vars['type']->ordinal(), 'ordinal',4);

out('* Propagation of new enum instance');

$vars = ['type'=>Type::get()];

dump_c($vars, ' $vars[] dump');

out($vars['type']->set(Type::SECOND)->name(), 'name',4);
out($vars['type']->set(Type::SECOND)->value(),'value',4);
out($vars['type']->set(Type::SECOND)->ordinal(), 'ordinal',4);

out('* setting propagated $vars[\'type\'] by ordinal');

out($vars['type']->byOrdinal(2)->name(), 'name by setting ordinal 2',4);
out($vars['type']->byOrdinal(2)->value(), 'value by setting ordinal 2',4);


?>