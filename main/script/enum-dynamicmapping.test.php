<?php
use Base\ENUM_PARAM;

require_once '../lib/helpers.php';
require_once '../lib/Enum.php';

injectCSS();

/**
 * Stubbed data
 * @author aaldert.vanweelden
 *
 */
class DB{

    private static $file = [
        [10,20,30],
        [100,200,300],
        [1000,2000,3000],
        [10000,20000,30000]
    ];

    public static function fetch($key){
        return self::$file[$key];
    }
}




/**
 * Config enumeration
 * @author aaldert.vanweelden
 *
 */
class TbbGroups extends Base\Enum{

    const FIRST = 'first_value';
	const SECOND = 'second_value';
	const THIRD = 'third_value';
	const FOURTH = 'fourth_value';

	public $default = self::FIRST;

	/* (non-PHPdoc)
	 * @see EnumInterface::get()
	 */
	public static function get($value = null, $default = ENUM_PARAM::ADD_DEFAULT_VALUE){
	    return new TbbGroups($value, $default);
	}

	/**
	 * Override
	 * {@inheritDoc}
	 * @see \Base\Enum::map()
	 */
	public function map(){

		if (self::$map===null){
		    out('* Creating the mapping only once...');
		    self::$map = [
		        self::FIRST => DB::fetch(0),
		        self::SECOND => DB::fetch(1),
		        self::THIRD => DB::fetch(2),
		        self::FOURTH => DB::fetch(3)
		    ];
		}
		return $this->mapping();
	}

}





/**
 * Model example
 * @author aaldert.vanweelden
 *
 */
class Model{

    /**
     *
     * @var array
     */
    private $config = [];

    public function __construct(){

        $this->config = array_merge( $this->config, ['tbb_groups' => TbbGroups::get()] );
    }

    /**
     *
     * @return array
     */
    public function getConfig(){
        return $this->config;
    }
}




/**
 * Controller example
 * @author aaldert.vanweelden
 *
 */
class Controller{

    /**
     *
     * @var Model
     */
    private $model;

    public function __construct(){
        $this->model = new Model();
    }

    public function template(){
        $config = $this->model->getConfig();

        dump_c( $config, 'The configuration array' );

        dump( $config['tbb_groups']->set(TbbGroups::SECOND)->name(), 'Second group name fetched from config' );
        dump( $config['tbb_groups']->set(TbbGroups::SECOND)->value(), 'Second group value fetched from config' );
        dump( $config['tbb_groups']->set(TbbGroups::SECOND)->ordinal(), 'Second group ordinal fetched from config' );

        dump_c( $config['tbb_groups']->set(TbbGroups::SECOND)->mapped(), 'Second group mapping fetched from config' );

        dump_c( $config['tbb_groups']->mapping(TbbGroups::SECOND), 'Second group mapping fetched from config by mapping key' );

        //not using model config
        dump( TbbGroups::get(TbbGroups::THIRD)->name(),  'Get the 3rd group name directly' );
        dump( TbbGroups::get(TbbGroups::THIRD)->value(),  'Get the 3rd group value directly' );
        dump_c( TbbGroups::get(TbbGroups::THIRD)->mapped(),  'Get the 3rd mapped group value directly' );
    }




}

(new Controller())->template();

?>