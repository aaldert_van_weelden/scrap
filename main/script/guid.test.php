<?php
require_once '../lib/helpers.php';

injectCSS();


class GUID{

	/**
	 * Create a unique version 4 GUID
	 * @param integer $seed
	 * @return string
	 */
	public static function create($seed = 0) {
		$microTime = microtime();
		list($a_dec, $a_sec) = explode(" ", $microTime);

		$seed += crc32(md5($microTime));
		mt_srand($seed);

		$dec_hex = sprintf("%x", $a_dec* 1000000 + $seed);
		$sec_hex = sprintf("%x", $a_sec + $seed);

		self::ensure_length($dec_hex, 5);
		self::ensure_length($sec_hex, 6);

		$guid = "";
		$guid .= $dec_hex;
		$guid .= self::create_guid_section(3);
		$guid .= '-';
		$guid .= self::create_guid_section(4);
		$guid .= '-';
		$guid .= self::create_guid_section(4);
		$guid .= '-';
		$guid .= self::create_guid_section(4);
		$guid .= '-';
		$guid .= $sec_hex;
		$guid .= self::create_guid_section(6);
		return $guid;
	}

	private static function create_guid_section($characters) {
		$return = "";
		for($i=0; $i<$characters; $i++) {
			$return .= sprintf("%x", mt_rand(0,15));
		}
		return $return;
	}

	private static function ensure_length(&$string, $length) {
		$strlen = strlen($string);
		if($strlen < $length) {
			$string = str_pad($string,$length,"0");
		}
		else if($strlen > $length){
			$string = substr($string, 0, $length);
		}
	}

}


dump(GUID::create());

dump(GUID::create(23445));


// $arr = [];
// $k = 0;
// for ($i=0; $i<100000; $i++) {
// 	$guid = GUID::create();
// 	array_key_exists($guid, $arr) ? $k++ : $arr[$guid] = true;
// }

//dump($k);


