<?php
require_once '../lib/helpers.php';

injectCSS();

/**
 * Data transport container
 * @author aaldert.vanweelden
 *
 */
class SecurityLogDTO{

    /**
     * Data transport container constructor
     * @param string $userName
     * @param integer $userId
     * @param integer $countryId
     */
    public function __construct($userName, $userId = null, $countryId = null){
        $this->userName = $userName;
        $this->userId = $userId;
        $this->countryId = $countryId;
    }

    public $userName;//mandatory
    public $userId;
    public $restId;
    public $countryId;
    public $accountNumber;
    public $accountName;
    public $orderId;
    public $invoiceId;
    public $subevent;//optional, eg. the form action
    public $data;//optional, eg. number of batch items
}
/**
 * Security log DAO and model
 * @author aaldert.vanweelden
 *
 */
class SecurityLogEntry{

    //persisted
    public $id;
    public $timestamp;
    public $sessionId;
    public $sourceIP;
    public $application;
    public $url;
    public $event;
    public $userId;
    public $userName;
    public $restId;
    public $countryId;
    public $accountNumber;
    public $accountName;
    public $orderId;
    public $invoiceId;
    public $ttl;
    public $keep;
    public $message;
    /**
     * @var PDO
     */
    private $pdo;
    private $threshold;
    private $report;

    public $subevent;
    public $data;
    public $map;
    public $types;

    public function __construct($event,$applicationName, $sessionID, $sourceIP, $referer){



        $this->id = null;
        $this->userId = null;
        $this->event = $event;
        $this->application = $applicationName;
        $this->sessionId = $sessionID;
        $this->sourceIP = $sourceIP;
        $this->url = $referer;
        $this->keep = 0;
        $this->timestamp = date("Y-m-d H:i:s");
        //$this->setTTL(0);

        $this->report = [];
        $this->map = [
            'timestamp' => 'timestamp',
            'sessionId' => 'session_id',
            'sourceIP' => 'source_ip',
            'application' => 'application',
            'url' => 'url',
            'event' => 'event',
            'userId' => 'user_id',
            'userName' => 'username',
            'restId' => 'rest_id',
            'countryId' => 'country_id',
            'orderId' => 'order_id',
            'invoiceId' => 'invoice_id',
            'accountNumber' => 'accountnumber',
            'accountName' => 'accountname',
            'ttl' => 'ttl',
            'keep' => 'keep',
            'message' => 'message',
            'data' => 'data'
        ];
        $this->types = [
            'timestamp' => PDO::PARAM_STR,
            'sessionId' => PDO::PARAM_STR,
            'sourceIP' => PDO::PARAM_STR,
            'application' => PDO::PARAM_STR,
            'url' => PDO::PARAM_STR,
            'event' => PDO::PARAM_STR,
            'userId' => PDO::PARAM_STR,
            'userName' => PDO::PARAM_STR,
            'restId' => PDO::PARAM_INT,
            'countryId' => PDO::PARAM_INT,
            'orderId' => PDO::PARAM_INT,
            'invoiceId' => PDO::PARAM_INT,
            'accountNumber' => PDO::PARAM_STR,
            'accountName' => PDO::PARAM_STR,
            'ttl' => PDO::PARAM_STR,
            'keep' => PDO::PARAM_INT,
            'message' => PDO::PARAM_STR,
            'data' => PDO::PARAM_STR
        ];
    }

    /**
     * Transfer DTO values to log
     * @param SecurityLogDTO $dto
     */
    public function setValues(SecurityLogDTO $dto){

        $data = get_object_vars($dto);
        foreach ($data as $key => $val){
            $this->$key = $val;
        }
    }

}

$dto = new SecurityLogDTO('aaldert');
$dto->restId = 123456;
$dto->accountNumber = 6789;
$dto->subevent = 'test';

$entry = new SecurityLogEntry('USER_TEST', 'TMS', '12', '10.53.1.8', 'http://localhost:8080/');
$entry->setValues($dto);

$params = ['userName', 'restId', 'accountNumber'];


$sql = "select count(*) as count";


$sql.= ", group_concat(id) as ids from tbb_log_monitoring where";
$i=0;
foreach ($params as $var){
    $sql.= " {$entry->map[$var]} = :$var";
    if(count($params)>1 && $i+1<count($params)){
        $sql.= " and";
    }
    $i++;
}

$bind = [];

foreach ($entry->map as $prop => $field){
    $bind[]=':'.$prop.' --> '.$entry->$prop.' , '.$entry->types[$prop];
}


$sql.= " and event = :event and ttl>now()";

out($sql);
dump($bind, 'bound params');

$sql = "insert into tbb_log_monitoring (".implode(", ",array_values($entry->map)).") values (:".implode(", :",array_keys($entry->map)).")";

out($sql);
$sub = [];
foreach($entry->map as $prop => $field){
    $sub[] = $field." = :".$prop;
}
$sql = "UPDATE tbb_log_monitoring
                    SET
                        ".implode(", ",$sub)."
                    WHERE id = :id
            ";


out($sql);

