<?php
require_once '../lib/helpers.php';

injectCSS();



function testFlow(){

	try {
		if(provide('error')){
			out('Provided Success');
			return true;
		}
		out('Provided Failure');
	}
	catch ( Exception $e) {
		out('Provided Exception');
	}
	return false;
}


function provide($mode){
	switch($mode){
		case 'success':
			return true;
		case 'failure':
			return false;
		case 'error':
			throw  new Exception('error');
	}
}


dump(testFlow());