<?php
require_once '../lib/helpers.php';

injectCSS();

$year = '2018';

$mysqlDate = date('Y-m-d H:i:s', strtotime($year));

dump($mysqlDate);

$pointer = '2007_1234';
$year = '2018-01-01 00:00:00';
$restId = 0;

if($pointer !== null){
	$spl = explode("_", $pointer);
	$year = $spl[0].'-01-01 00:00:00';
	if(key_exists(1, $spl)) $restId = (integer) $spl[1];
}

dump($year, 'year');
dump($restId, 'restId');

$timestamp  = date("Y-m-d H:i:s");

out('* convert timestamp '.$timestamp.' to logstash format [18/Sep/2017:18:01:16 +0200] ');


$offset = getOffset('Europe/Amsterdam');

out($offset, 'offset');

$iso8601_offset =

$convert = date("[d/M/Y:H:i:s ".$offset."]", time($timestamp));

dump($convert);

out('* test padding');

out(  str_pad( 0,  2 , "0", STR_PAD_LEFT ), 'padding the number', 4);


$tzObj = timezone('Europe/Amsterdam');

dump($tzObj);


/**
 * Return the transition object if available, use
 * @param object  $timezone
 */
function timezone($timezone){
    $tz = new DateTimeZone($timezone);

    $transitions = $tz->getTransitions();
    if (is_array($transitions)){
        foreach ($transitions as $k => $t){
            // look for current year
            if (substr($t['time'],0,4) == date('Y')){
                $trans = $t;
                break;
            }
        }
    }
    return (isset($trans)) ? (object) $trans : false;
}

/**
 * Return the offset in seconds
 * Returns null if the DST is not available for detecting (PHP version < 5.2)
 */
function getOffset($dateTimezone = self::DATETIMEZONE){
    $tz = timezone($dateTimezone);
    if(is_object($tz)){
        $sign = (integer) $tz->offset<0?"-":"+";
        $hour = str_pad(floor(abs($tz->offset)/3600),  2 , "0", STR_PAD_LEFT );

        return $sign.$hour.'00';
    }
    return '+0000';
}

out('---------------------------------  formatting ---------------------------------------');

dump(date('Y-m-d', strtotime('2018-06-12 23:11:34')));

dump( date('Y-m-d'));