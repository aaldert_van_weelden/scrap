<?php
require_once '../lib/helpers.php';

injectCSS();

/**
 * Data transport container
 * @author aaldert.vanweelden
 *
 */
class SecurityLogDTO{

    /**
     * Data transport container constructor
     * @param string $userName
     * @param integer $userId
     * @param integer $countryId
     */
    public function __construct($userName, $userId = null, $countryId = null){
        $this->userName = $userName;
        $this->userId = $userId;
        $this->countryId = $countryId;
    }

    public $userName;//mandatory
    public $userId;
    public $restId;
    public $countryId;
    public $accountNumber;
    public $accountName;
    public $orderId;
    public $invoiceId;
    public $subevent;//optional, eg. the form action
    public $data;//optional, eg. number of batch items
}
/**
 * Security log DAO and model
 * @author aaldert.vanweelden
 *
 */
class SecurityLogEntry{

    /**
     * Incremental database id
     * @name id
     * @var string
     */
    public $id;
    /**
     * Creation datetime Y-m-d H:i:s
     * @name timestamp
     * @var string
     */
    public $timestamp;
    /**
     * PHP session
     * @name session_id
     * @var string
     */
    public $sessionId;
    /**
     * Client IP address
     * @name source_ip
     * @var string
     */
    public $sourceIP;
    /**
     * Application name uppercase
     * @name application
     * @var string
     */
    public $application;
    /**
     * Url from HTTP_REFERER
     * @name url
     * @var string
     */
    public $url;
    /**
     * The fired event
     * @name event
     * @var string
     */
    public $event;
    /**
     * User id available when logged in
     * @name user_id
     * @var integer
     */
    public $userId;
    /**
     * Mandatory username
     * @name username
     * @var string
     */
    public $userName;
    /**
     * Restaurant id
     * @name rest_id
     * @var integer
     */
    public $restId;
    /**
     * @name country_id
     * @var integer
     */
    public $countryId;
    /**
     * Payment account
     * @name accountnumber
     * @var string
     */
    public $accountNumber;
    /**
     * Customer account name
     * @name accountname
     * @var string
     */
    public $accountName;
    /**
     * @name order_id
     * @var integer
     */
    public $orderId;
    /**
     * @name invoice_id
     * @var integer
     */
    public $invoiceId;
    /**
     * Time to live timestamp Y-m-d H:i:s
     * @name ttl
     * @var string
     */
    public $ttl;
    /**
     * Keep as evidence when TRUE
     * @name keep
     * @var boolean
     */
    public $keep;
    /**
     * Optional event message
     * @name message
     * @var string
     */
    public $message;
    /**
     * Optional event data
     * @name data
     * @var string
     */
    public $data;


    /**
     * @var PDO
     */
    private $pdo;
    private $threshold;
    private $report;

    public $subevent;
    public $map;
    public $types;

    public function __construct($event,$applicationName, $sessionID, $sourceIP, $referer){



        $this->id = null;
        $this->userId = null;
        $this->event = $event;
        $this->application = $applicationName;
        $this->sessionId = $sessionID;
        $this->sourceIP = $sourceIP;
        $this->url = $referer;
        $this->keep = false;
        $this->timestamp = date("Y-m-d H:i:s");
        //$this->setTTL(0);
        $this->setMapping();

        $this->report = [];
    }

    /**
     * Transfer DTO values to log
     * @param SecurityLogDTO $dto
     */
    public function setValues(SecurityLogDTO $dto){

        $data = get_object_vars($dto);
        foreach ($data as $key => $val){
            $this->$key = $val;
        }
    }

    public function setMapping(){
        $refClass = new ReflectionClass($this);
        foreach ($refClass->getProperties() as $refProperty) {
            if (preg_match('/@var\s+([^\s]+)/', $refProperty->getDocComment(), $matches)) {
                list(, $type) = $matches;
                if(in_array($type, ['array','object'])){
                    continue;
                }
            }
            if (preg_match('/@name\s+([^\s]+)/', $refProperty->getDocComment(), $matches)) {
                list(, $name) = $matches;
            }else{
                continue;
            }
            $this->map[$name] = $refProperty->name;

            switch($type){
                case 'integer':
                case 'int':
                    $this->types[$name] = PDO::PARAM_INT;
                    break;
                case 'boolean':
                case 'bool':
                    $this->types[$name] = PDO::PARAM_BOOL;
                    break;
                case 'NULL':
                    $this->types[$name] = PDO::PARAM_NULL;
                    break;
                default:
                    $this->types[$name] = PDO::PARAM_STR;
                    break;
            }
        }
    }

}

$dto = new SecurityLogDTO('aaldert');
$dto->restId = 123456;
$dto->accountNumber = 6789;
$dto->subevent = 'test';

$entry = new SecurityLogEntry('USER_TEST', 'TMS', '12', '10.53.1.8', 'http://localhost:8080/');
$entry->setValues($dto);

$bind = [];

foreach ($entry->map as $field => $prop){
    $bind[]=':'.$prop.' --> '.(empty($entry->$prop)?'NULL':$entry->$prop).' , '.$entry->types[$field];
}


dump($bind, 'bound params');


dump($entry->map);
dump($entry->types);

$sql01 = "insert into tbb_log_monitoring (" . implode(", ", array_keys($entry->map)) . ") values (:" . implode(", :", array_values($entry->map)) . ")";

$sub = [];
foreach ($entry->map as $field => $prop) {
    $sub[] = $field . " = :" . $prop;
}
$sql02 = "UPDATE tbb_log_monitoring SET " . implode(", ", $sub) . " WHERE id = :id";

out($sql01);
out($sql02);



