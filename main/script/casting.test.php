<?php
declare(strict_types=1);

require_once '../lib/helpers.php';

injectCSS();




$test = [null, 1,2,0, "23" , "z", true , false, "bar", "false"];

foreach ($test as $val){

	$val = (integer) $val;
	var_dump($val);
}
print '<br>';

foreach ($test as $val){

	$val = (boolean) $val;
	var_dump($val);
}

print '<br>';

foreach ($test as $val){

	$val = (string) $val;
	var_dump($val);
}

print '<br>';

foreach ($test as $val){

	$val = (array) $val;
	var_dump($val);
}

foreach ($test as $key => $val){
	$test[$key] = (integer) $val;
}

var_dump($test);

out('------------------------- constructor typing --------------------------------');



class ConstructType {

// 	public function __construct(?string $in, string $str){
// 		var_dump($in, $str);
// 	}
}

$test = new ConstructType('hi','test');

$test = new ConstructType(12,'test');

$test = new ConstructType('hi',12);


