<?php
require_once '../lib/helpers.php';
require_once '../lib/Enum.php';

use Base\Enum;
use Base\ENUM_CMP;
use Base\ENUM_PARAM;

injectCSS();

class Type extends Enum{

	const FIRST = 'FIRST_VALUE';
	const SECOND = 'A_SECOND_VALUE';
	const THIRD = 'THIRD_VALUE';

	public $default = self::FIRST;

	/* (non-PHPdoc)
	 * @see EnumInterface::get()
	 */
	public static function get($setValue = null, $default = ENUM_PARAM::ADD_DEFAULT_VALUE){
	    return new Type($setValue, $default);
	}
}

class Test extends Enum{

	const FIRST = 'FIRST_VALUE';
	const SECOND = 'A_SECOND_VALUE';
	const THIRD = 'THIRD_VALUE';

	public $default = self::FIRST;

	/* (non-PHPdoc)
	 * @see EnumInterface::get()
	 */
	public static function get($setValue = null, $default = ENUM_PARAM::ADD_DEFAULT_VALUE){
	    return new Test($setValue, $default);
	}
}

//-----------------------------------------
//type to compare to

$type = Type::get(Type::SECOND);

dump($type->constants(), 'reference key-values pairs');

$test = Test::get();

out('* checking the enums for equality');

dump($type->equals($test), 'Non-strict equality check  (default) , only the KV-pairs');

dump($type->equals($test, true), 'Strict (type-aware) equality check by object');

out(Type::SECOND,'Compare with ');

//-------------------------------------------

out('* Test::__default = null');

$test->value(null);

show();

//---------------------------------------------------

out('* Test::FIRST = '.Test::FIRST);

$test->value(Test::FIRST);

show();
//---------------------------------------------------

out('* Test::SECOND = '.Test::SECOND);

$test->value(Test::SECOND);

show();
//---------------------------------------------------

out('* Test::THIRD = '.Test::THIRD);

$test->value(Test::THIRD);

show();
//---------------------------------------------------

function show(){
    global $type;
    global $test;

    dump($type->compare($test),'Comparing by ordinals (default)');

    dump($type->compare($test,ENUM_CMP::INT),'Comparing the enum values as int');

    dump($type->compare($test, ENUM_CMP::STR),'Comparing the enum string values');

    dump($type->equals($test->value), 'Non-strict equality check  (default) by value');

    dump($type->equals($test->value, true), 'Strict (type-aware) equality check by value');

}

?>