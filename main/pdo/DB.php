<?php

class DB extends PDO
{

	protected static $instance = null;

	protected static $host = 'localhost';
	protected static $port = 3306;
	protected static $user = 'root';
	protected static $pass = '';
	protected static $db ='pdo_test';
	protected static $path = '';

	function __construct($dsn, $username = null, $password = null, $driver_options = array()) {

		try {
			parent::__construct($dsn, $username, $password, $driver_options);
		} catch (Exception $e) {
			echo 'Database not available';
			trigger_error($e, E_USER_ERROR);
		}
	}

	/**
	 * Get the DB connector singleton
	 * @return NULL|DB
	 */
	public static function instance() {

		if (! is_object(self::$instance)) {

			self::$instance = new DB('mysql:dbname=' . self::$db . ';host=' . self::$host . ';port=' . self::$port, self::$user, self::$pass, [
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
			]);

			$stmt = self::$instance->query("SET NAMES UTF8");
		}
		return self::$instance;
	}


	public static function connectMySQL(){

		$res =  @mysql_connect(self::$host.':'.self::$port, self::$user, self::$pass);
		mysql_select_db(self::$db, $res);
		mysql_set_charset('utf8', $res);
		return $res;

	}



	public static function connected() {
		return is_object(self::$instance) and self::$instance;
	}

	public static function disconnect() {
		self::$instance = null;
	}

	public function __clone() {
		throw new Exception('Cannot clone ' . __CLASS__ . ' class');
	}

}
