<?php
use Base\Enum;
use Base\ENUM_PARAM;

require_once __DIR__.'/../lib/Enum.php';
require_once __DIR__.'/../lib/helpers.php';
require_once 'simpletest/autorun.php';
require_once 'testdata.php';

injectCSS();

/**
 * Put the ENUM class  tests here
 *
 * @author Aaldert van Weelden
 *
 */
class EnumWithDefaultValue extends Enum
{
	const ONE = 1;
	const TWO = 2;
	public $value = 1;

    public static function get($value = null, $default = ENUM_PARAM::ADD_DEFAULT_VALUE ){
        return new EnumWithDefaultValue($value, $default);
    }




}

class EnumWithNullAsDefaultValue extends Enum
{
	const NONE = null;
	const ONE  = 1;
	const TWO  = 2;

	public static function get($value = null, $default = ENUM_PARAM::ADD_DEFAULT_VALUE ){
	    return new EnumWithNullAsDefaultValue($value, $default);
	}
}


class EnumWithoutDefaultValue extends Enum
{
	const ONE = 1;
	const TWO = 2;

	public static function get($value = null, $default = ENUM_PARAM::ADD_DEFAULT_VALUE ){
	    return new EnumWithoutDefaultValue($value, $default);
	}
}

class EnumInheritance extends EnumWithoutDefaultValue
{
	const INHERITACE = 'Inheritance';

	public static function get($value = null, $default = ENUM_PARAM::ADD_DEFAULT_VALUE ){
	    return new EnumInheritance($value, $default);
	}
}

class EmptyEnum extends Enum
{
    public static function get($value = null, $default = ENUM_PARAM::ADD_DEFAULT_VALUE ){
        return new EmptyEnum($value, $default);
    }
}

/**
 * Tests start here
 * @author Aaldert van Weelden
 *
 */

class EnumTest extends UnitTestCase {



	public function __construct(){
		print '<br><u>'.dirname(__FILE__).DIRECTORY_SEPARATOR.get_class($this) . "</u>:<br> \n";
	}

	public function __destruct(){

	}

	/**
	 * Called before every test
	 */
	public function setup(){

	}

	/**
	 * Called after every test
	 */
	public function teardown(){

	}

	public function testEnumWithDefaultValue(){
		print __FUNCTION__ . " :<br>\n";

		$enum = EnumWithDefaultValue::get();

		dump_c($enum, 'EnumWithDefaultValue dump');

		$this->assertEqual(array(
		        '__default' => null,
				'ONE' => 1,
				'TWO' => 2,
		), (array)$enum->constants());

		$this->assertEqual(1, $enum->value());
		$this->assertEqual(1, $enum->__invoke());

		$this->assertEqual('ONE', $enum->name());
		$this->assertEqual('ONE', $enum->__toString());
	}

	public function testEnumWithNullAsDefaultValue(){
		print __FUNCTION__ . " :<br>\n";

		$enum = EnumWithNullAsDefaultValue::get(null,ENUM_PARAM::NO_DEFAULT_VALUE);

		$this->assertEqual(array(
				'NONE' => null,
				'ONE'  => 1,
				'TWO'  => 2,
		), (array)$enum->constants());

		$this->assertNull($enum->value());
		$this->assertNull($enum->__invoke());

		$this->assertEqual('NONE', $enum->name());
		$this->assertEqual('NONE', $enum->__toString());
	}

	public function testEnumWithoutDefaultValue(){
		print __FUNCTION__ . " :<br>\n";

		//@expected InvalidArgumentException
		try{
			new EnumWithoutDefaultValue();
		}catch(InvalidArgumentException $e){
			print "Expected exception : ".$e->getMessage()."\n<br>";
			$this->pass();
		}

	}

	public function testEnumInheritance(){
		print __FUNCTION__ . " :<br>\n";

		$enum = EnumInheritance::get(EnumInheritance::ONE);

		try{
		$this->assertEqual(EnumInheritance::ONE, $enum->value());
		}catch(InvalidArgumentException $e){
			print "Expected exception : ".$e->getMessage()."\n<br>";
			$this->pass();
		}


	}

	public function testChangeValueOnConstructor(){
		print __FUNCTION__ . " :<br>\n";

		$enum = EnumWithoutDefaultValue::get(1);

		$this->assertEqual(1, $enum->value());
		$this->assertEqual(1, $enum->__invoke());

		$this->assertEqual('ONE', $enum->name());
		$this->assertEqual('ONE', $enum->__toString());
	}

	public function testChangeValueOnConstructorThrowsInvalidArgumentExceptionOnStrictComparison(){
		print __FUNCTION__ . " :<br>\n";

		//@expected Exception
		try{
			$enum = EnumWithoutDefaultValue::get('1');
		}catch(Exception $e){
			print "Expected exception : ".$e->getMessage()."\n<br>";
			$this->pass();
		}
	}

	public function testSetValue(){
		print __FUNCTION__ . " :<br>\n";

		$enum = EnumWithDefaultValue::get();
		$enum->set(2);

		$this->assertEqual(2, $enum->value());
		$this->assertEqual(2, $enum->__invoke());

		$this->assertEqual('TWO', $enum->name());
		$this->assertEqual('TWO', $enum->__toString());
	}

	public function testSetValueThrowsInvalidArgumentExceptionOnStrictComparison(){
		print __FUNCTION__ . " :<br>\n";

		//@expected ArgumentException
		try{
			$enum = EnumWithDefaultValue::get();
			$enum->set('2');
		}catch(Exception $e){
			print "Expected exception : ".$e->getMessage()."\n<br>";
			$this->pass();
		}
	}

}

?>