<?php
require_once __DIR__.'/../lib/helpers.php';

injectCSS();

$_POST['name'] = 'test';
$_POST['id'] = 1;
$_POST['productId'] = 100;
$_POST['price'] = 4;
$_POST['product'] = 'bread';
$_POST['stock'] = '4';
$_POST['description'] = 'test description';
$_POST['img'] = 'test.jpg';
$_POST['stock'] = '7';

	$productId = $_POST['productId'];
	foreach ($_POST as $key => $value) {
		if (!in_array($key,array('action','key','productId'))) {
			$update[$key] = strip_tags(trim($value));
		}
	}
	$update['price'] = number_format(preg_replace('/[^0-9.]/','',str_replace(',','.',$update['price'])),2,'.','');
	$update['img'] = preg_replace('/[^a-z0-9._\-]/','',mb_strtolower($update['img']));
	if (!in_array(array_pop(explode('.',$update['img'])),array('png','jpg','gif'))) {
		$update['img'] = '';
	}
	$update['stock'] = (int) $update['stock'];


	if ($update['product'] and $update['description']) {



		$bindings = array();
		$statements = array();
		foreach ($update as $key => $value) {
			if ($key == 'id') {
				continue;
			}
			$bindings[":$key"] = $value;
			$statements[] = "`$key`=:$key";
		}
		$bindings[":productId"] = $productId;
		# update product
		$sql = "UPDATE merchandise SET ".implode(',',$statements)." WHERE id=:productId LIMIT 1";
		//$stmt = DB::getInstance()->prepare($sql);
		//$stmt->execute($bindings);


		dump($sql);

		//$stmt = DB::getInstance()->prepare($sql);
		//$stmt->bindParam(':productId', $productId, PDO::PARAM_INT);
		foreach($bindings as $key => $value){
			if(is_numeric($value)){
				out($key.' => '.$value.'    INT');
			}else{
				out($key.' => '.$value.'   STR');
			}

		}
		$stmt->execute();
		# update sortIdCat
		$sql = "UPDATE merchandise SET sortIdCat = :sortIdCat WHERE category = :productId";
		$stmt = DB::getInstance()->prepare($sql);
		$stmt->bindParam(':sortIdCat', $update['sortIdCat'], PDO::PARAM_INT);
		$stmt->bindParam(':productId', $update['category'], PDO::PARAM_STR);
		$stmt->execute();
		unset($update);
		$report['update'] = '<p class="successText">Product updated.</p>';
	} else {
		$report['update'] = '<p class="errorText">Required fields missing.</p>';
	}

	dump($report);
