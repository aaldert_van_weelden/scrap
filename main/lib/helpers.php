<?php
function out($in,$message = '', $tabs = 0){

	print '<br>';
	for($i=0;$i<=$tabs;$i++){
	    print '&nbsp;';
	}
	if($tabs>0)print'&#9633;&nbsp;';
	print !empty($message)?$message.' : ':'';
	print $in===null?'NULL':$in;
	print '<br><br>';
}

function dump($in, $message=''){

	print '<br>';
	print get_log_sender();
	print ' : ';
	print !empty($message)?$message.' : ':'';
	//print '<pre>';
	ob_start();
	echo chr(10).chr(10);var_dump($in);echo chr(10);
	$result = ob_get_contents();
	ob_end_clean();
	print $result;

	print '<br>';
}

$dumpCount = 0;
$instanceCount = 0;
/**
 * Helper to dump the content to screen, can be expanded and collapsed
 * @param object $object
 * @param string $caption
 * @param boolean $halt
 */
function dump_c($object, $caption=null, $halt=false){
    global $dumpCount;
    global $instanceCount;

    $dumpCount++;
    $ID = $dumpCount.'-'.$instanceCount;
    print '<br>';
    print get_log_sender();
    print ' : ';
    print '<span style="display:visible;cursor:pointer;" id="plus_'.$ID.'" onclick="
			document.getElementById(\'plus_'.$ID.'\').style.display=\'none\';
			document.getElementById(\'minus_'.$ID.'\').style.display=\'inline\';
			document.getElementById(\'dump_'.$ID.'\').style.display=\'block\';
		">[+]&nbsp;</span>';
    print '<span style="display:none;cursor:pointer;" id="minus_'.$ID.'" onclick="
			document.getElementById(\'plus_'.$ID.'\').style.display=\'inline\';
			document.getElementById(\'minus_'.$ID.'\').style.display=\'none\';
			document.getElementById(\'dump_'.$ID.'\').style.display=\'none\';
		">[-]&nbsp;</span>';
    if($caption!=null){
        print '<b>'.$caption.' : </b><br>';
    }
    print '<span style="display:none;" class="util_dump" id="dump_'.$ID.'">';
    print '<pre>';
    var_dump($object);
    print '</pre>';
    print '</span>';
    print '<br>';
    if($halt){
        exit();
    }
}



function injectCSS(){
	print'
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style>
	body, pre{font-family:consolas;font-size:14px;}
	.xdebug-var-dump{display:inline;}
	h3{float:left;}
	.header{float:left;font-size:12px;position:relative;left:12px; top:3px;}
	small{font-size:0.5em;display:none}

</style></head><body><h3><u>DEBUG</u></h3><span class="header">'.get_log_sender().'</span><div style="clear:both"></div>';
}


function get_log_sender(){
	$backtrace = debug_backtrace();
	$index=1;

	if($backtrace[$index]!=null && is_array($backtrace[$index])){
		if(isset($backtrace[$index]['file'])){
			$file = $backtrace[$index]['file'];
		}else{
			return 'no sender detected';
		}

		if(isset($backtrace[$index]['line'])){
			$line = $backtrace[$index]['line'];
		}else{
			return 'no sender detected';
		}


		if(strpos($file,'/')>-1){
			$arr_file = explode('/',$file);
		}else if(strpos($file,'\\')>-1){
			$arr_file = explode('\\',$file);
		}else{
			return $file." line ".$line;
		}
		$count = count($arr_file);
		if($count>1){
			return $arr_file[$count-2].'/'. $arr_file[$count-1]." line ".$line;
		}else if($count==1){
			return $arr_file[0]." line ".$line;
		}else{
			return $file." line ".$line;
		}

		return $file." line ".$line;
	}
	return 'no sender detected';
}



