<?php  namespace Base;

interface EnumInterface{

    /**
    * Create and return an instance of the enum with the provided value
    *
    * @param mixed $val  Optional , default NULL
    * @param boolean $default  If ENUM_PARAM::ADD_DEFAULT_VALUE the default value is added.
    * <br> Values: ENUM_PARAM::NO_DEFAULT_VALUE | ENUM_PARAM::ADD_DEFAULT_VALUE  (default)
    * @return Enum
    */
    public static function get($value = null, $default = ENUM_PARAM::ADD_DEFAULT_VALUE);
}
