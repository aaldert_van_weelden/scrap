<?php
/**
 * Use options --start and --stop for begin- and endpoints, format mySQL date yy-mm-dd<br>
 * Processing data in chunks with 10 seconds pause in between to let others catchup<br>
 * Outputs the datetime pointer in a logfile in order to be able to  manually resume processing after system failure
 */
error_reporting(E_ALL);
ini_set('display_errors', 1);

$options = getopt('edlr:f:',['offline','pointer']);
if (!$options)
	$options = array();

	/**
	 * <u>Optional</u>
	 * File logger prefix, {LOGPREFIX}_debug.log,  {LOGPREFIX}_error.log
	 * @see self::log()
	 * @see self::error()
	 * @var string
	 */
	define ('LOGPREFIX', 'fixrestvotes');

	define ( 'DEBUGGING', array_key_exists ( 'd', $options ) );

	define ( 'EXECUTING', array_key_exists ( 'e', $options ) );

	define ( 'LIVE', array_key_exists ( 'l', $options ) );

	define('OFFLINE', array_key_exists('offline', $options));


	define ( 'country', 'de' );
	define ( 'country_code', '2' );

	define ('takeaway_host', 'LOCAL');

	//var_dump($options);
	//define pointer
	$pointer = (array_key_exists('pointer', $options)) ? $options['pointer'] : null;

	$pointer = "2012_5";

	//var_dump($pointer);die();


	$fdrst = new FixRestaurantVotes ( $pointer );
	$fdrst::debug ( "(DEBUGGING = " . (DEBUGGING ? 'true' : 'false') . ")", 's' );
	$fdrst->run ();
	echo 'DONE!' . PHP_EOL;

	class FixRestaurantVotes {

		const MAX_YEAR = 2018;//from
		const MIN_YEAR = 2009;//included
		/**
		 * If TRUE the log writes the message to debug also
		 * @var string
		 */
		const DEBUG_OUT = true;


		/**
		 * The list of restaurant ids
		 * @var array
		 */
		private $restIds;

		/**
		 * Pointer {year}_{rest_id}
		 * Start year to process (2018 -> 2009)  or NULL
		 * @var integer
		 */
		private $year;
		/**
		 * Pointer {year}_{rest_id}
		 * Start restaurant.rest_id to process ASC  or NULL
		 * @var integer
		 */
		private $restId;
		/**
		 * Pointer {year} | {year}_{rest_id}
		 * @var string
		 */
		private $pointer;
		/**
		 * The list of pointers
		 * @var array
		 */
		private $pointers;

		/**
		 * Constructor
		 * @param string $pointer The pointer where to start.  {year}  or {year}_{rest_id}
		 */
		public function __construct( $pointer ) {


			//queries
			$actief = OFFLINE?0:1;
			$countryCode = country_code;



			$this->year = self::MAX_YEAR;
			$this->rest_id = 0;
			$this->restIds = [1,2,3,4,5,6,7,8,9,10];//test
			$this->pointer = null;
			$this->pointers = [];

			if($pointer !== null){
				self::log("setting pointer $pointer", self::DEBUG_OUT);
				$this->pointer = $pointer;
				$spl = explode("_", $this->pointer);
				$this->year = (integer) $spl[0];
				if(key_exists(1, $spl)) $this->restId = (integer) $spl[1];
			}
		}

		public function run() {

			$this->iterateRestaurantBlocks();

			$this->exportPointers();

		}


		private function iterateRestaurantBlocks(){
			self::log('Iterating the restaurant blocks',self::DEBUG_OUT);
			//process the restaurant list per year
			for($year = $this->year; $year > self::MIN_YEAR; $year--){

				$restToProcess = [];
				$pointer = '';

				if($this->pointer !== null && $year === $this->year){
					$pointer = 'Pointer provided. ';
					foreach ($this->restIds as $id){
						if($id >= $this->restId){
							$restToProcess[] = $id;
						}
					}
				}else{
					$restToProcess= $this->restIds;
				}
				self::log($pointer."Iterating the restaurants starting from $this->restId and ascending from $year to ".self::MIN_YEAR,self::DEBUG_OUT);

				foreach ($restToProcess as $restId){

					self::log("processing votes for restaurant $restId and year $year", self::DEBUG_OUT);
					$this->appendPointer($year, $restId);

					$fromYear = ($year-1).'-01-01 00:00:00';
					$untilYear = $year.'-01-01 00:00:00';

					self::log("processing votes: from $fromYear to $untilYear", self::DEBUG_OUT);
				}
				sleep(1);
			}
		}


		private function appendPointer($year, $restId){
			$this->pointers[] = $year.'_'.$restId;
		}

		private function exportPointers(){
			$now = date("YmdHis");
			$csv = fopen(LOGPREFIX."_pointers_$now.csv", "a");
			fputs($csv, "pointer\r\n");
			foreach($this->pointers as $entry){
				fputs($csv, $entry."\r\n");
			}
			fclose($csv);
		}

		/*
		 |------------------------------------------------------------------------------------
		 |		HELPER FUNCTIONS BELOW
		 |------------------------------------------------------------------------------------
		 */

		public static function debug($message, $type = false) {
			if (DEBUGGING || $type != false) {
				if ($type == 'e') {
					$message = "\033[31m" . $message . "\033[0m";
				} else if ($type == 's') {
					$message = "\033[32m" . $message . "\033[0m";
				}
				echo "$message\n";
			}
		}


		/**
		 * File logger
		 * @param string $message
		 * @param string $tag
		 * @return boolean
		 */
		public static function log($message, $debugOut = false) {

			if($debugOut)self::debug($message);

			$prefix = defined('LOGPREFIX')?LOGPREFIX.'_':'';

			$msg = date("Y-m-d H:i:s ") . " ";

			$msg .= str_pad(takeaway_host,20);//IP

			$msg .= str_pad('DEBUG',6);
			$msg .= ": ";

			$msg .=  " ".$message . "\n";

			$logfile = fopen($prefix.'debug.log', "a");
			fputs($logfile, $msg);
			fclose($logfile);
			return true;
		}


		/**
		 * File logger
		 * @param string $message
		 * @param string $tag
		 * @return boolean
		 */
		public static function error($error, $tag = '') {

			$prefix = defined('LOGPREFIX')?LOGPREFIX.'_':'';

			$msg = date("Y-m-d H:i:s ") . " ";

			$msg .= str_pad(takeaway_host,20);//IP

			$msg .= str_pad('ERROR',6);
			$msg .= ": ";

			$msg .= $tag;
			$msg .= ':';

			$msg .=  " ".$error . "\n";

			$logfile = fopen($prefix.'error.log', "a");
			fputs($logfile, $msg);
			fclose($logfile);
			return true;
		}


		/**
		 * File logger
		 * @param mixed $object
		 * @param string $tag
		 * @return boolean
		 */
		public static function dump($object, $tag = '') {

			$prefix = defined('LOGPREFIX')?LOGPREFIX.'_':'';

			$msg = date("Y-m-d H:i:s ") . " ";

			$msg .= str_pad(takeaway_host,20);//IP

			$msg .= str_pad('DUMP',6);
			$msg .= ": ";

			$msg .= $tag;
			$msg .= ':';

			ob_start();
			echo chr(10).chr(10);var_dump($object);echo chr(10);
			$dump = ob_get_contents();
			ob_end_clean();

			$msg .=  " ".$dump . "\n";

			$logfile = fopen($prefix.'debug.log', "a");
			fputs($logfile, $msg);
			fclose($logfile);
			return true;
		}
	}

